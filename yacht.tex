\documentclass{article}
\usepackage{bigpage}
\usepackage{blockpar}
\usepackage{blocktitle}
\usepackage{czt}
\usepackage{url}

\newcommand{\Yah}{Yacht}

\title{{\Yah}: A Yahtzee Variant}
\author{Bart Massey}
\date{29 March 2016}


\begin{document}
\maketitle

\section{Introduction}

Yahtzee is a game played with five dice. We will describe a
variant of Yahtzee known as {\Yah}. {\Yah} is an older game
(\url{https://en.wikipedia.org/wiki/Yacht_(dice_game)}) with
slightly simpler rules.

{\Yah} is built around a scoresheet. A scoresheet
holds scores for various entries (combinations of dice in a
die roll). The scoresheet can be filled in in any order.

\begin{zed}
  POSN == 1 \upto 5 \\
  FACE == 1 \upto 6 \\
  ENTRY ::= ks \ldata FACE \rdata | full\_house | four\_of\_a\_kind | \\
  \t1 little\_straight | big\_straight | choice | yacht
\end{zed}

\begin{schema}{Scoresheet}
  scoresheet: ENTRY \pfun \nat
\end{schema}

A {\Yah} game starts with an empty scoresheet.

\begin{schema}{InitYah}
  Scoresheet
\where
  scoresheet = \emptyset
\end{schema}

We will describe a roll of five dice in terms of the values
on each of the five top faces.

\begin{axdef}
  ROLL : \power~\seq~FACE
\where
  \forall r : ROLL @ \dom r = POSN
\end{axdef}

The sum of the range values of a binary relation will be
useful throughout this specification. It should arguably be
part of the toolkit---oh well.

\begin{gendef}[X]
  sum\_range : (X \rel \num) \fun \num
\where
  sum\_range(\emptyset) = 0 \\
  \forall x : X; n : \num; xns : X \rel \num @ \\
  \t1 sum\_range(\{x \mapsto n\} \cup xns) = n + sum\_range(xns)
\end{gendef}

\section{Scoring}

We will describe the scores of {\Yah} entries for now in
terms of a scoring function. For a given category, the
scoring function assigns a positive score if the roll mets
the category description, and assigns a zero score
otherwise.

To score a roll as Aces, Twos, Threes etc, we sum the
faces with the designated value.

\begin{zed}
  SCORE == ENTRY \cross ROLL \pfun \nat
\end{zed}

\begin{axdef}
  score\_ks : SCORE
\where
  score\_ks = \{ k : FACE; r : ROLL @ \\
  \t1 (ks(k) , r) \mapsto sum\_range(r \rres \{k\}) \}
\end{axdef}

To score a roll as a Full House, we give it the sum of all
its faces if three of the faces match and the two remaining
faces also match; zero otherwise. Yacht may be
scored as a Full House.

\begin{axdef}
  zero : ENTRY \fun SCORE
\where
  \forall e : ENTRY @ zero(e) = \{r : ROLL @ (e, r) \mapsto 0\} 
\end{axdef}

\begin{axdef}
  score\_full\_house : SCORE
\where
  score\_full\_house = zero(full\_house) \oplus \{ r : ROLL; x : \power POSN | \\
  \t1 \# x = 3 \land
  \# (r \limg x \rimg) = 1 \land \# (r \limg POSN \setminus x \rimg) = 1 @ \\
  \t1 (full\_house, r) \mapsto  sum\_range(r) \}
\end{axdef}

To score a roll as Four of a Kind, we give it a score equal
to the sum of its matching faces if there are at least four
matching faces, and zero otherwise. A Yacht may be scored as
a Four of a Kind, but receives points only for four of the
faces.

\begin{axdef}
  score\_four\_of\_a\_kind : SCORE
\where
  score\_four\_of\_a\_kind = zero(four\_of\_a\_kind) \oplus
  \{ r : ROLL; x : \power POSN | \\
  \t1 \# x = 4 \land \# (r \limg x \rimg) = 1 @ \\
  \t1 (four\_of\_a\_kind, r) \mapsto  sum\_range(x \dres r) \}
\end{axdef}

To score a roll as a Little Straight (a better name would be
Low Straight), we give it 30 points iff its faces in some
order are $1 \upto 5$.

\begin{axdef}
  score\_little\_straight : SCORE
\where
  score\_little\_straight = zero(little\_straight) \oplus
  \{r : ROLL | \ran(r) = 1 \upto 5 @ \\
  \t1 (little\_straight, r) \mapsto  30\}
\end{axdef}

To score a roll as a Big Straight (a better name would be
High Straight), we give it 30 points iff its faces in some
order are $2 \upto 6$.

\begin{axdef}
  score\_big\_straight : SCORE
\where
  score\_big\_straight = zero(big\_straight) \oplus
  \{r : ROLL | \ran(r) = 2 \upto 6 @ \\
  \t1 (big\_straight, r) \mapsto  30\}
\end{axdef}

To score a roll as Choice, we score it as the sum of its
faces.

\begin{axdef}
  score\_choice : SCORE
\where
  score\_choice = \{r : ROLL @ (choice, r) \mapsto sum\_range(r)\}
\end{axdef}

To score a roll as Yacht, we give it 50 points if all five
dice are the same and 0 points otherwise.

\begin{axdef}
  score\_yacht : SCORE
\where
  score\_yacht = zero(yacht) \oplus \{r : ROLL | \# \ran(r) = 1 @ 
   (yacht, r) \mapsto 50\}
\end{axdef}

The overall score is simply the score obtained by 
filling in the value of the given roll at the given
entry.

\begin{axdef}
  score : ENTRY \cross ROLL \fun \nat
\where
  score = score\_ks \cup score\_full\_house \cup 
      score\_four\_of\_a\_kind \cup \\
  \t1 score\_little\_straight \cup score\_big\_straight \cup \\
  \t1 score\_choice \cup score\_yacht
\end{axdef}

\section{Play}

A turn in {\Yah} consists of rolling five dice, possibly
rerolling once or twice, selecting an entry that matches the
final roll, and filling in the scoresheet with the score of
that match. An alternative is always to fill in a zero score
in any scoresheet entry.

\begin{schema}{Dice}
  roll : ROLL \\
  count : \nat
\where
  count \leq 2
\end{schema}

At the start of each turn, an initial roll is made.

\begin{schema}{InitTurn}
  Dice~' \\
  \Xi Scoresheet \\
  roll? : ROLL
\where
  \dom scoresheet \subset ENTRY \\
  roll' = roll? \\
  count' = 2
\end{schema}
  
Once the initial roll has been made, the player may select
any of the five dice to reroll. This can happen up to
twice. Another way to look at it is that the player must
twice reroll some subset of the dice, but may choose to
reroll none. We will adopt this view, which leads to an
easier description.

\begin{schema}{Reroll}
  \Delta Dice \\
  \Xi Scoresheet \\
  replace? : POSN \pfun FACE
\where
  count > 0 \\
  count' = count - 1 \\
  roll' = roll \oplus replace?
\end{schema}

When the rerolls are complete, the player selects an empty
entry to fill in with the score.

\begin{schema}{EndTurn}
  Dice \\
  \Delta Scoresheet \\
  choice? : ENTRY
\where
  count = 0 \\
  choice? \notin \dom scoresheet \\
  scoresheet' = scoresheet \cup \{ choice? \mapsto score (choice?, roll) \}
\end{schema}

When all scoresheet entries are filled in, the game is over,
and the score is the sum of the values of scoresheet
entries.

\begin{schema}{EndYah}
  Scoresheet \\
  final\_score! : \nat
\where
  \dom scoresheet = ENTRY \\
  final\_score! = sum\_range(scoresheet)
\end{schema}  

The game is played from beginning to end.

\begin{zed}
  YahGame == InitYah~' \semi (InitTurn \semi Reroll \lor
  EndTurn) \lor EndYah
\end{zed}

\end{document}
